package com.example.tradeapp

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.util.Size
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.myapplication.BottomNavigation1
import com.example.myapplication.forthPage
import com.example.myapplication.homePage
import com.example.myapplication.onTradePage
import com.example.myapplication.settingPage
import com.example.myapplication.thirdPage
import com.example.myapplication.topAppBar
import com.example.myapplication.ui.theme.MyApplicationTheme
import com.example.tradeapp.R

class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {


            val navState = rememberNavController()
            val showTopAppBar = remember { mutableStateOf(true) }
            val NumberofLR = remember { mutableStateOf(1) }
            var aa = remember {
                mutableStateOf(1)
            }


            MyApplicationTheme {
                Scaffold(
                    modifier = Modifier.fillMaxSize(),
                    bottomBar = { BottomNavigation1(navState,showTopAppBar.value, aa) },
                    topBar = {topAppBar(navState,showTopAppBar.value)}
                )
                {
                    NavHost(navController = navState, startDestination = "homePage") {
                        showTopAppBar.value = true
                        composable(route = "homePage") {
                            var selectedCard by remember { mutableStateOf(0) }
                            homePage(navState)
                            showTopAppBar.value = true
                            aa.value = 1

                        }
                        composable(route="onTradePage"){
                            showTopAppBar.value = true
                            onTradePage(navState)
                            aa.value = 2
                        }

                        composable(route="settingPage"){
                            showTopAppBar.value = false
                            settingPage(navState)
                        }

                        composable(route="thirdPage"){
                            showTopAppBar.value = true
                            thirdPage(navState)
                            aa.value = 3
                        }
                        composable(route="forthPage"){
                            showTopAppBar.value = true
                            forthPage(navState)
                            aa.value = 4
                        }
                    }
                }


            }

        }
    }




    override fun onResume() {
        super.onResume()
        when(this.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)){
            Configuration.UI_MODE_NIGHT_NO -> {
                window.statusBarColor = resources.getColor(R.color.white, null)
            }
            else -> {
                window.statusBarColor = resources.getColor(R.color.black, null)
            }
        }
    }
}

//



