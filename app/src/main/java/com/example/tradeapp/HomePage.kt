package com.example.myapplication

import android.annotation.SuppressLint
import android.view.MotionEvent
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.color1
import com.example.myapplication.ui.theme.diagramColor
import kotlinx.coroutines.launch
import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.drawText
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.fontscaling.MathUtils.lerp
import com.example.tradeapp.R
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlin.math.PI
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin
import kotlin.math.sqrt

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CircularSlider(
    modifier: Modifier = Modifier,
    padding: Float = 50f,
    stroke: Float = 10f,
    cap: StrokeCap = StrokeCap.Round,
    touchStroke: Float = 50f,
    thumbColor: Color = Color.Blue,
    progressColor: Color = Color.Black,
    backgroundColor: Color = Color(0xFF4E5461),
    debug: Boolean = false,
    onChange: ((Float)->Unit)? = null
){
    var appliedAngle by remember { mutableStateOf(0f) }

    val shakeAnimation = remember {
        Animatable(initialValue = 0f)
    }

    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(Unit) {
        while (true) {
            coroutineScope.launch {
                shakeAnimation.animateTo(
                    targetValue = if (shakeAnimation.value == 0f) 10f else 218f,
                    animationSpec = keyframes {
                        durationMillis = 500
                        0f at 0 using LinearEasing // Starting position
                        10f at 250 using LinearEasing // Maximum rotation to the right
                        -10f at 500 using LinearEasing // Maximum rotation to the left
                        0f at 750 using LinearEasing // Back to the starting position
                    }
                )
            }
            delay(2000)
        }
    }
    var width by remember { mutableStateOf(0) }
    var height by remember { mutableStateOf(0) }
    var angle by remember { mutableStateOf(-60f) }
    var last by remember { mutableStateOf(0f) }
    var down  by remember { mutableStateOf(false) }
    var radius by remember { mutableStateOf(0f) }
    var center by remember { mutableStateOf(Offset.Zero) }


    Canvas(
        modifier = modifier
            .onGloballyPositioned {
                width = it.size.width
                height = it.size.height
                center = Offset(width / 2f, height / 2f)
                radius = min(width.toFloat(), height.toFloat()) / 2f - padding - stroke / 2f
            }

    ){
        //bozorg
        drawArc(
            color = backgroundColor,
            startAngle = -240f,
            sweepAngle = 360f,
            topLeft = center - Offset(radius,radius),
            size = Size(radius*2,radius*2),
            useCenter = false,
            style = Stroke(
                width = stroke,
                cap = cap
            )
        )
        //hala
        val rotationOffset = shakeAnimation.value

        drawCircle(
            color = Color(0xFF8A3F5A),
            radius = 3.dp.toPx(),
            center = Offset(
                radius*cos((200+appliedAngle)*PI/80f).toFloat()+ rotationOffset ,
                radius*sin((215+appliedAngle)*PI/205f).toFloat()
            )
        )
        drawCircle(
            color = Color(0xFFFF2C7B),
            radius = stroke,
            center = center + Offset(
                radius*cos((180+appliedAngle)*PI/180f).toFloat(),
                radius*sin((180+appliedAngle)*PI/180f).toFloat()
            )
        )
        drawCircle(
            color = Color(0xFF657CFD),
            radius = stroke,
            center = center + Offset(
                radius*cos((350+appliedAngle)*PI/180f).toFloat(),
                radius*sin((350+appliedAngle)*PI/180f).toFloat()
            )
        )
        drawCircle(
            color = Color(0xFFF86902),
            radius = stroke,
            center = center + Offset(
                radius*cos((370+appliedAngle)*PI/180f).toFloat(),
                radius*sin((370+appliedAngle)*PI/180f).toFloat()
            )
        )
        drawCircle(
            color = thumbColor,
            radius = stroke,
            center = center + Offset(
                radius*cos((220+appliedAngle)*PI/180f).toFloat(),
                radius*sin((220+appliedAngle)*PI/180f).toFloat()
            )
        )
        drawCircle(
            color = Color(0xFFEA973D),
            radius = stroke,
            center = center + Offset(
                radius*cos((230+appliedAngle)*PI/180f).toFloat(),
                radius*sin((230+appliedAngle)*PI/180f).toFloat()
            )
        )
        drawCircle(
            color = Color.Red,
            radius = stroke,
            center = center + Offset(
                radius*cos((340+appliedAngle)*PI/180f).toFloat(),
                radius*sin((340+appliedAngle)*PI/180f).toFloat()
            )
        )
        drawCircle(
            color = Color.Yellow,
            radius = stroke,
            center = center + Offset(
                radius*cos((320+appliedAngle)*PI/180f).toFloat(),
                radius*sin((320+appliedAngle)*PI/180f).toFloat()
            )
        )
      //  kochik
        drawCircle(
            color = Color.Cyan,
            radius = stroke,
            center = center + Offset(
                radius*cos((120+appliedAngle)*PI/180f).toFloat(),
                radius*sin((120+appliedAngle)*PI/180f).toFloat()
            )
        )
        //2
        drawCircle(
            color = Color(0xFF00AF45),
            radius = stroke,
            center = center + Offset(
                radius*cos((150+appliedAngle)*PI/180f).toFloat(),
                radius*sin((150+appliedAngle)*PI/180f).toFloat()
            )
            //3


        )

    }
}

fun angle(center: Offset, offset: Offset): Float {
    val rad = atan2(center.y - offset.y, center.x - offset.x)
    val deg = Math.toDegrees(rad.toDouble())
    return deg.toFloat()
}
fun distance(first: Offset, second: Offset) : Float{
    return sqrt((first.x-second.x).square()+(first.y-second.y).square())
}
fun Float.square(): Float{
    return this*this
}

@SuppressLint("RestrictedApi")
@Composable
fun CustomRectangle() {
    val width = 320.dp
    val height = 111.dp


    val transition = rememberInfiniteTransition()
    val animatedFraction by transition.animateFloat(
        initialValue = 0f,
        targetValue = 1f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 5000),
            repeatMode = RepeatMode.Restart
        )
    )

    Box(modifier = Modifier.size(width, height)) {
        Canvas(modifier = Modifier.fillMaxSize()) {
            val verticalLines = 4
            val verticalSize = (size.width * 1.1) / (verticalLines + 1)
            val horizontalLines = 2
            val barWidthPx = 1.dp.toPx()
            val chartColor = Color(0xB2FFC90E)
            val brushColor = Color(0x48FFC90E).copy(0.3f)
            val dataPoints = listOf(
                0.2f, 0.9f, 0.5f, 0.8f, 1f, 2f, 1.6f, 0.8f, 0.2f, 1.2f, 1.4f, 0.6f, 0.9f,
                0.3f, 0.246f, 0.87f, 0.4f, 0.653f, 0.442f, 0.328f
            )

            // Draw left line
            drawLine(
                color = Color(0xFF292D3B),
                start = Offset(0f, 0f),
                end = Offset(0f, size.height),
                strokeWidth = 2f
            )

            // Draw top line
            drawLine(
                color = Color(0xFF292D3B),
                start = Offset(0f, 0f),
                end = Offset(size.width, 0f),
                strokeWidth = 2f
            )

            // Draw lower line
            drawLine(
                color = Color(0xFF292D3B),
                start = Offset(0f, size.height),
                end = Offset(size.width, size.height),
                strokeWidth = 2f
            )

            repeat(verticalLines) { i ->
                val startX = verticalSize * (i + 1)
                drawLine(
                    color = Color(0xFF292D3B),
                    start = Offset(startX.toFloat(), 0f),
                    end = Offset(startX.toFloat(), size.height),
                    strokeWidth = barWidthPx
                )
            }

            val sectionSize = size.height / (horizontalLines + 1)
            repeat(2) { i ->
                val startY = sectionSize * (i + 1)
                drawLine(
                    color = Color(0xFF292D3B),
                    start = Offset(0f, startY),
                    end = Offset(size.width, startY),
                    strokeWidth = barWidthPx
                )
            }

            // Calculate animated path
            val path = Path()
            val xStep = size.width / (dataPoints.size - 1)
            val yStep = size.height / 2
            path.moveTo(0f, size.height - dataPoints[0] * yStep)
            dataPoints.forEachIndexed { i, dataPoint ->
                val x = i * xStep
                val y = size.height - dataPoint * yStep
                if (i < dataPoints.size - 1) {
                    val nextX = (i + 1) * xStep
                    val nextY = size.height - dataPoints[i + 1] * yStep
                    val controlX = (x + nextX) / 2
                    val controlY = lerp(y, nextY, animatedFraction)
                    path.quadraticBezierTo(controlX, controlY, nextX, nextY)
                }
            }
            path.lineTo(size.width, size.height)
            path.lineTo(0f, size.height)
            path.close()

            // Draw animated path
            drawPath(
                brush = Brush.verticalGradient(
                    colors = listOf(chartColor, brushColor ,Color.Transparent),
                    startY = 0f,
                    endY = size.height
                ),
                path = path
            )
        }
    }
}
@Composable
fun customCard(modifier: Modifier = Modifier,map: Map<String, Any>, isSelected: Boolean, onClick: () -> Unit) {
    // Spacer(modifier = Modifier.height(500.dp))
    Row(modifier = Modifier
        .padding(start = 10.dp)
        .systemBarsPadding()) {
        Box(
            modifier = Modifier
                .width(339.dp)
                .height(200.dp)
            //   .background(Color.Yellow)

        ) {
            val cardcolor = MaterialTheme.colorScheme.primary


            Canvas(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                val startX = 0f
                val startY = 30f
                val endX = size.width
                val endY = 30f
                val strokeWidth = 4f

                val controlX = (startX + endX) / 2
                val controlY = startY - 50f

                val path = Path().apply {
                    moveTo(startX, startY)
                    quadraticBezierTo(controlX, controlY, endX, endY)
                }

                drawPath(
                    path = path,
                    color = Color.Black,
                    style = Stroke(width = strokeWidth)
                )

                drawLine(
                    color = Color.Black,
                    start = Offset(0f, 30f),
                    end = Offset(0f, size.height-30),
                    strokeWidth = 4f
                )
                drawLine(
                    color = Color.Black,
                    start = Offset(size.width, 30f),
                    end = Offset(size.width, size.height-30),
                    strokeWidth = 4f
                )
                //s
                val startX2 = 0f
                val startY2 = size.height -30
                val endX2 = size.width
                val endY2 = size.height-30
                val strokeWidth2 = 4f

                val controlX2 = (startX2 + endX2) / 2
                val controlY2 = startY2 + 50f

                val path2 = Path().apply {
                    moveTo(startX2, startY2)
                    quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                }

                drawPath(
                    path = path2,
                    color = Color.Black,
                    style = Stroke(width = strokeWidth2)
                )


                val enclosingPath = Path().apply {
                    moveTo(startX, startY)
                    quadraticBezierTo(controlX, controlY, endX, endY)
                    lineTo(endX2, endY2)
                    quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                    close()
                }

                drawPath(
                    path = enclosingPath,
                    color = cardcolor,
                    style = Fill
                )

            }
            Box(modifier.padding(top = 10.dp, start = 10.dp)) {
                Column(
                    Modifier
                        .fillMaxSize()
                        .padding(start = 20.dp, end = 20.dp, top = 20.dp)) {
                    Row(Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween) {
                        Text(
                            text = "Total Balance",
                            color = MaterialTheme.colorScheme.onPrimary,
                            fontFamily = Sora,
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Normal
                        )
                        Icon(

                            painter = painterResource(id = R.drawable.g1),
                            modifier =
                            Modifier
                                .size(23.dp)
                                .height(10.dp)
                                .width(10.dp),
                            contentDescription = null,
                            tint = Color(0xFF4B5166)
                        )

                    }
                    //s
                    Row(Modifier.fillMaxWidth()) {
                        Text(
                            text = map["Num1"] as String,
                            color = MaterialTheme.colorScheme.onPrimary,
                            fontFamily = Sora,
                            fontSize = 23.sp,
                            fontWeight = FontWeight.Medium
                        )
                        Text(
                            text = map["Num2"] as String,
                            color = MaterialTheme.colorScheme.onPrimary,
                            fontFamily = Sora,
                            fontSize = 23.sp,
                            fontWeight = FontWeight.Thin
                        )

                    }
                    Row {
                        Text(
                            text = map["Num3"] as String,
                            color = MaterialTheme.colorScheme.onPrimary,
                            fontFamily = Sora,
                            fontSize = 12.sp,
                            fontWeight = FontWeight.Normal
                        )
                        Spacer(modifier = Modifier.width(30.dp))
                        Row(
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = Alignment.CenterVertically
                        ) {

                            Text(
                                text = map["Num4"] as String,
                                color = Color.Red,
                                fontFamily = Sora,
                                fontSize = 12.sp,
                                fontWeight = FontWeight.Normal
                            )

                            Spacer(modifier = Modifier.width(10.dp))

                            Icon(

                                painter = painterResource(id = R.drawable.b3),
                                modifier =
                                Modifier
                                    .height(6.dp)
                                    .width(12.dp),
                                contentDescription = null,
                                tint = Color.Red

                            )

                        }



                    }
                    Spacer(Modifier.height(20.dp))

                    //s
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                    ) {
                        var selected by remember {
                            mutableStateOf(1)
                        }


                        Box(
                            modifier = Modifier
                                .width(140.dp)
                                .height(30.dp)
                                .clip(RoundedCornerShape(20.dp))
                                .background(if (selected == 1) Color(0xFF4B5166) else Color.Transparent)
                                .clickable { selected = 1 },
                            contentAlignment = Alignment.Center
                        )
                        {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth(),
                                horizontalArrangement = Arrangement.SpaceEvenly,
                                verticalAlignment = Alignment.CenterVertically
                            ) {

                                Icon(

                                    painter = painterResource(id = R.drawable.plus),
                                    modifier =
                                    Modifier
                                        .size(15.dp),
                                    contentDescription = null,
                                    tint = Color.White

                                )
                                Text(
                                    text = "Deposit",
                                    color = if(selected==1)Color.White else MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 16.sp,
                                    fontWeight = FontWeight.Normal
                                )

                            }


                        }
                        //s

                        var isChecked by remember { mutableStateOf(true) }
                        Box(
                            modifier = Modifier
                                .width(140.dp)
                                .height(30.dp)
                                .clip(RoundedCornerShape(20.dp))
                                .background(if (selected == 2) Color(0xFF4B5166) else Color.Transparent)
                                .clickable {
                                    selected = 2
                                    isChecked = true
                                },
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = "Withdraw",
                                color = if(selected==2)Color.White else MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                    }
                }
            }
        }
    }
}
//f1
@Composable
fun numberOfLR2(selectedCard: Int, currentScrollPosition: Int) {
    val itemWidth = 900

    Box(Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        Row {
            repeat(3) { index ->
                Box(
                    modifier = Modifier
                        .width(if (currentScrollPosition >= index * itemWidth && currentScrollPosition < (index + 1) * itemWidth) 30.dp else 7.dp)
                        .height(7.dp)
                        .clip(RoundedCornerShape(3.5.dp))
                        .background(MaterialTheme.colorScheme.primary)
                        .shadow(
                            elevation = 40.dp,
                            shape = RoundedCornerShape(8.dp),
                            clip = true
                        )
                        .padding(horizontal = 2.dp),
                    contentAlignment = Alignment.Center
                ) {

                }
                Spacer(modifier = Modifier.width(3.dp))
            }
        }
    }
}
@Composable
fun MyScreen2(cardsData: List<Map<String, Any>>, selectedCard: Int, onCardSelected: (Int) -> Unit) {
    val scrollState = rememberScrollState()
    var currentScrollPosition by remember { mutableStateOf(0) }
    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(selectedCard) {
        coroutineScope.launch {
            scrollState.animateScrollTo(selectedCard * 100)
        }
    }

    scrollState.apply {
        currentScrollPosition = value
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 16.dp, top = 16.dp)
    ) {
        Row(
            modifier = Modifier
                .horizontalScroll(scrollState)
        ) {
            cardsData.forEachIndexed { index, map ->
                customCard(
                    modifier = Modifier,
                    map = map,
                    isSelected = selectedCard == index,
                    onClick = { onCardSelected(index) }
                )
                Spacer(modifier = Modifier.width(16.dp))
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        numberOfLR2(selectedCard = selectedCard, currentScrollPosition)
    }
}

//myworld2
@Composable
fun homePage(navController: NavController, NumberofLR: Int = 1) {
    val cardData = listOf(
        mapOf(
            "Num1" to "234",
            "Num2" to "45674523  BTC",
            "Num3" to "10,554.88",
            "Num4" to "1.455%",
        ),
        mapOf(
            "Num1" to "451",
            "Num2" to "79675423  BTC",
            "Num3" to "22,612.44",
            "Num4" to "1.562%",
        ),
        // Add more card maps as needed

        mapOf(
            "Num1" to "729",
            "Num2" to "37684126  BTC",
            "Num3" to "37,237.21",
            "Num4" to "1.127%",
        ),
    )


    Column(
        Modifier
            .systemBarsPadding()
            .background(MaterialTheme.colorScheme.surface)
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(start = 5.dp, top = 20.dp, bottom = 70.dp)
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        Spacer(modifier = Modifier.height(16.dp))

        var selectedCard by remember { mutableStateOf(0) }
        MyScreen2(cardsData = cardData, selectedCard = selectedCard) {
            selectedCard = it
        }
        //az injaha
        Spacer(modifier = Modifier.height(10.dp))
        Box(
            Modifier
                .systemBarsPadding()
                .fillMaxWidth()
                .height(400.dp)
                .padding(start = 25.dp, end = 25.dp)
        ) {
            val cardcolor = MaterialTheme.colorScheme.primary

            Canvas(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                val startX = 0f
                val startY = 30f
                val endX = size.width
                val endY = 30f
                val strokeWidth = 4f

                val controlX = (startX + endX) / 2
                val controlY = startY - 50f

                val path = Path().apply {
                    moveTo(startX, startY)
                    quadraticBezierTo(controlX, controlY, endX, endY)
                }

                drawPath(
                    path = path,
                    color = Color.Black,
                    style = Stroke(width = strokeWidth)
                )

                drawLine(
                    color = Color.Black,
                    start = Offset(0f, 30f),
                    end = Offset(0f, size.height - 30),
                    strokeWidth = 4f
                )
                drawLine(
                    color = Color.Black,
                    start = Offset(size.width, 30f),
                    end = Offset(size.width, size.height - 30),
                    strokeWidth = 4f
                )
                //s
                val startX2 = 0f
                val startY2 = size.height - 30
                val endX2 = size.width
                val endY2 = size.height - 30
                val strokeWidth2 = 4f

                val controlX2 = (startX2 + endX2) / 2
                val controlY2 = startY2 + 50f

                val path2 = Path().apply {
                    moveTo(startX2, startY2)
                    quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                }

                drawPath(
                    path = path2,
                    color = Color.Black,
                    style = Stroke(width = strokeWidth2)
                )


                val enclosingPath = Path().apply {
                    moveTo(startX, startY)
                    quadraticBezierTo(controlX, controlY, endX, endY)
                    lineTo(endX2, endY2)
                    quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                    close()
                }

                drawPath(
                    path = enclosingPath,
                    color = cardcolor,
                    style = Fill
                )

            }
            //n1

            Box(
                modifier = Modifier

                    .fillMaxWidth()
                    .clip(RoundedCornerShape(15.dp))

                //.height(308.dp)
            ){
                Column(Modifier.fillMaxWidth()) {
                    var selectedItem by remember {
                        mutableStateOf(1)
                    }

                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(20.dp)
                    ) {

                        Row(
                            modifier = Modifier
                                .fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = "In Order:",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Normal
                                )
                                Text(
                                    text = " 256.45 BTC",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 14.sp,
                                    fontWeight = FontWeight.Medium
                                )
                            }

                            LinearProgressIndicator(
                                progress = 0.4f ,
                                modifier = Modifier
                                    .size(90.dp, 5.dp)
                                    .clip(
                                        RoundedCornerShape(100)
                                    ),
                                color = Color(0xFFFFC90E)
                            )

                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        //s row 2
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween
                        ){
                            Row(
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = "Daily Limit",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Normal
                                )
                                Text(
                                    text = " $75,000",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 14.sp,
                                    fontWeight = FontWeight.Medium
                                )
                                Spacer(modifier = Modifier.width(12.dp))
                                var iconColor by remember { mutableStateOf(Color.White) }
                                var isClicked by remember { mutableStateOf(false) }

                                LaunchedEffect(isClicked) {
                                    if (isClicked) {
                                        iconColor = Color.Blue
                                        delay(200)
                                        iconColor = Color.White
                                        isClicked = false
                                    }
                                }

                                Box(
                                    modifier = Modifier
                                        .size(25.dp)
                                        .clickable { isClicked = true }

                                )
                                {
                                    Canvas(
                                        modifier = Modifier
                                            .fillMaxSize()
                                    ) {
                                        val startX = 0f
                                        val startY = 2f
                                        val endX = size.width
                                        val endY = 2f
                                        val strokeWidth = 0.5f

                                        val controlX = (startX + endX) / 2
                                        val controlY = startY - 20f

                                        val path = Path().apply {
                                            moveTo(startX, startY)
                                            quadraticBezierTo(controlX, controlY, endX, endY)
                                        }

                                        drawPath(
                                            path = path,
                                            color = Color.Black,
                                            style = Stroke(width = strokeWidth)
                                        )
                                        //line left
                                        val startX3 = 0f
                                        val startY3 = 2f
                                        val endX3 = 0f
                                        val endY3 = size.height-2f
                                        val strokeWidth3 = 0.5f
                                        val controlX3 = startX3 -20f
                                        val controlY3 = (startY3 + endY3) / 2
                                        val path3 = Path().apply {
                                            moveTo(startX3, startY3)
                                            quadraticBezierTo(controlX3, controlY3, endX3, endY3)
                                        }
                                        drawPath(
                                            path = path3,
                                            color = Color.Black,
                                            style = Stroke(width = strokeWidth3)
                                        )
                                        //line right
                                        val startX4 = size.width
                                        val startY4 = 2f
                                        val endX4 = size.width
                                        val endY4 = size.height-2f
                                        val strokeWidth4 = 0.5f
                                        val controlX4 = startX4 +20f
                                        val controlY4 = (startY4 + endY4) / 2
                                        val path4 = Path().apply {
                                            moveTo(startX4, startY4)
                                            quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                        }
                                        drawPath(
                                            path = path4,
                                            color = Color.Black,
                                            style = Stroke(width = strokeWidth4)
                                        )
                                        //down
                                        val startX2 = 0f
                                        val startY2 = size.height -2f
                                        val endX2 = size.width
                                        val endY2 = size.height-2f
                                        val strokeWidth2 = 0.5f

                                        val controlX2 = (startX2 + endX2) / 2
                                        val controlY2 = startY2 + 20f

                                        val path2 = Path().apply {
                                            moveTo(startX2, startY2)
                                            quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                                        }

                                        drawPath(
                                            path = path2,
                                            color = Color.Black,
                                            style = Stroke(width = strokeWidth2)
                                        )

                                        val enclosingPath = Path().apply {
                                            moveTo(startX, startY)
                                            quadraticBezierTo(controlX, controlY, endX, endY)
                                            lineTo(startX4, startY4)
                                            quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                            lineTo(endX2, endY2)
                                            quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                                            lineTo(startX3, startY3)
                                            quadraticBezierTo(controlX3, controlY3, endX3, endY3)
                                            close()
                                        }

                                        drawPath(
                                            path = enclosingPath,
                                            color = Color(0xFF4B5166),
                                            style = Fill
                                        )
                                    }
                                    Icon(
                                        painter = painterResource(id = R.drawable.bb0),
                                        modifier = Modifier.
                                        size(25.dp),
                                        contentDescription = null,
                                        tint = iconColor
                                    )
                                }
                            }
                            LinearProgressIndicator(
                                progress = 0.8f ,
                                modifier = Modifier
                                    .size(90.dp, 5.dp)
                                    .clip(
                                        RoundedCornerShape(100)
                                    ),
                                color = Color(0xFFFF2C7B)
                            )


                        }

                    }
                    Divider(
                        color = Color(0xCC818181),
                        thickness = 1.dp,
                        modifier = Modifier
                            .fillMaxWidth()

                    )
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(20.dp)
                    ) {
                        Row(Modifier.fillMaxWidth()) {
                            Box(
                                modifier = Modifier
                                    .size(25.dp)
                                    .clickable { selectedItem = 1 }
                            )
                            {
                                Canvas(
                                    modifier = Modifier
                                        .fillMaxSize()
                                ) {
                                    val startX = 0f
                                    val startY = 2f
                                    val endX = size.width
                                    val endY = 2f
                                    val strokeWidth = 0.5f

                                    val controlX = (startX + endX) / 2
                                    val controlY = startY - 20f

                                    val path = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                    }

                                    drawPath(
                                        path = path,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth)
                                    )
                                    //line left
                                    val startX3 = 0f
                                    val startY3 = 2f
                                    val endX3 = 0f
                                    val endY3 = size.height-2f
                                    val strokeWidth3 = 0.5f
                                    val controlX3 = startX3 -20f
                                    val controlY3 = (startY3 + endY3) / 2
                                    val path3 = Path().apply {
                                        moveTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)
                                    }
                                    drawPath(
                                        path = path3,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth3)
                                    )
                                    //line right
                                    val startX4 = size.width
                                    val startY4 = 2f
                                    val endX4 = size.width
                                    val endY4 = size.height-2f
                                    val strokeWidth4 = 0.5f
                                    val controlX4 = startX4 +20f
                                    val controlY4 = (startY4 + endY4) / 2
                                    val path4 = Path().apply {
                                        moveTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                    }
                                    drawPath(
                                        path = path4,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth4)
                                    )
                                    //down
                                    val startX2 = 0f
                                    val startY2 = size.height -2f
                                    val endX2 = size.width
                                    val endY2 = size.height-2f
                                    val strokeWidth2 = 0.5f

                                    val controlX2 = (startX2 + endX2) / 2
                                    val controlY2 = startY2 + 20f

                                    val path2 = Path().apply {
                                        moveTo(startX2, startY2)
                                        quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                                    }

                                    drawPath(
                                        path = path2,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth2)
                                    )


                                    val enclosingPath = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                        lineTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                        lineTo(endX2, endY2)
                                        quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                                        lineTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)

                                        close()
                                    }

                                    drawPath(
                                        path = enclosingPath,
                                        color = if (selectedItem==1)Color(0xFFA5611A) else Color(0xFF4B5166),
                                        style = Fill
                                    )
                                }
                                Icon(

                                    painter = painterResource(id = R.drawable.bb1),
                                    modifier =
                                    Modifier
                                        .size(25.dp),
                                    contentDescription = null,
                                    tint = Color.White
                                )

                            }
                            Spacer(modifier = Modifier.width(15.dp))
                            //2
                            Box(
                                modifier = Modifier
                                    .size(25.dp)
                                    .clickable { selectedItem = 2 }
                            )
                            {
                                Canvas(
                                    modifier = Modifier
                                        .fillMaxSize()
                                ) {
                                    val startX = 0f
                                    val startY = 2f
                                    val endX = size.width
                                    val endY = 2f
                                    val strokeWidth = 0.5f

                                    val controlX = (startX + endX) / 2
                                    val controlY = startY - 20f

                                    val path = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                    }

                                    drawPath(
                                        path = path,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth)
                                    )
                                    //line left
                                    val startX3 = 0f
                                    val startY3 = 2f
                                    val endX3 = 0f
                                    val endY3 = size.height-2f
                                    val strokeWidth3 = 0.5f
                                    val controlX3 = startX3 -20f
                                    val controlY3 = (startY3 + endY3) / 2
                                    val path3 = Path().apply {
                                        moveTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)
                                    }
                                    drawPath(
                                        path = path3,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth3)
                                    )
                                    //line right
                                    val startX4 = size.width
                                    val startY4 = 2f
                                    val endX4 = size.width
                                    val endY4 = size.height-2f
                                    val strokeWidth4 = 0.5f
                                    val controlX4 = startX4 +20f
                                    val controlY4 = (startY4 + endY4) / 2
                                    val path4 = Path().apply {
                                        moveTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                    }
                                    drawPath(
                                        path = path4,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth4)
                                    )
                                    //down
                                    val startX2 = 0f
                                    val startY2 = size.height -2f
                                    val endX2 = size.width
                                    val endY2 = size.height-2f
                                    val strokeWidth2 = 0.5f

                                    val controlX2 = (startX2 + endX2) / 2
                                    val controlY2 = startY2 + 20f

                                    val path2 = Path().apply {
                                        moveTo(startX2, startY2)
                                        quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                                    }

                                    drawPath(
                                        path = path2,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth2)
                                    )


                                    val enclosingPath = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                        lineTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                        lineTo(endX2, endY2)
                                        quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                                        lineTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)

                                        close()
                                    }

                                    drawPath(
                                        path = enclosingPath,
                                        color = if (selectedItem==2)Color(0xFF2C9957) else Color(0xFF4B5166),
                                        style = Fill
                                    )
                                }
                                Icon(

                                    painter = painterResource(id = R.drawable.bb2),
                                    modifier =
                                    Modifier
                                        .size(25.dp)
                                        .offset(y = 1.dp),
                                    contentDescription = null,
                                    tint = Color(0xFF04CA65)
                                )

                            }
                            Spacer(modifier = Modifier.width(15.dp))
                            //3
                            Box(
                                modifier = Modifier
                                    .size(25.dp)
                                    .clickable { selectedItem = 3 }
                            )
                            {
                                Canvas(
                                    modifier = Modifier
                                        .fillMaxSize()
                                ) {
                                    val startX = 0f
                                    val startY = 2f
                                    val endX = size.width
                                    val endY = 2f
                                    val strokeWidth = 0.5f

                                    val controlX = (startX + endX) / 2
                                    val controlY = startY - 20f

                                    val path = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                    }

                                    drawPath(
                                        path = path,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth)
                                    )
                                    //line left
                                    val startX3 = 0f
                                    val startY3 = 2f
                                    val endX3 = 0f
                                    val endY3 = size.height-2f
                                    val strokeWidth3 = 0.5f
                                    val controlX3 = startX3 -20f
                                    val controlY3 = (startY3 + endY3) / 2
                                    val path3 = Path().apply {
                                        moveTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)
                                    }
                                    drawPath(
                                        path = path3,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth3)
                                    )
                                    //line right
                                    val startX4 = size.width
                                    val startY4 = 2f
                                    val endX4 = size.width
                                    val endY4 = size.height-2f
                                    val strokeWidth4 = 0.5f
                                    val controlX4 = startX4 +20f
                                    val controlY4 = (startY4 + endY4) / 2
                                    val path4 = Path().apply {
                                        moveTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                    }
                                    drawPath(
                                        path = path4,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth4)
                                    )
                                    //down
                                    val startX2 = 0f
                                    val startY2 = size.height -2f
                                    val endX2 = size.width
                                    val endY2 = size.height-2f
                                    val strokeWidth2 = 0.5f

                                    val controlX2 = (startX2 + endX2) / 2
                                    val controlY2 = startY2 + 20f

                                    val path2 = Path().apply {
                                        moveTo(startX2, startY2)
                                        quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                                    }

                                    drawPath(
                                        path = path2,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth2)
                                    )


                                    val enclosingPath = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                        lineTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                        lineTo(endX2, endY2)
                                        quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                                        lineTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)

                                        close()
                                    }

                                    drawPath(
                                        path = enclosingPath,
                                        color = if (selectedItem==3)Color(0xFF2D3B8D) else Color(0xFF4B5166),
                                        style = Fill
                                    )
                                }
                                Icon(

                                    painter = painterResource(id = R.drawable.bb3),
                                    modifier =
                                    Modifier
                                        .size(25.dp),
                                    contentDescription = null,
                                    tint = Color(0xFF627EF8)
                                )

                            }
                            Spacer(modifier = Modifier.width(15.dp))
                            //4
                            Box(
                                modifier = Modifier
                                    .size(25.dp)
                                    .clickable { selectedItem = 4 }
                            )
                            {
                                Canvas(
                                    modifier = Modifier
                                        .fillMaxSize()
                                ) {
                                    val startX = 0f
                                    val startY = 2f
                                    val endX = size.width
                                    val endY = 2f
                                    val strokeWidth = 0.5f

                                    val controlX = (startX + endX) / 2
                                    val controlY = startY - 20f

                                    val path = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                    }

                                    drawPath(
                                        path = path,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth)
                                    )
                                    //line left
                                    val startX3 = 0f
                                    val startY3 = 2f
                                    val endX3 = 0f
                                    val endY3 = size.height-2f
                                    val strokeWidth3 = 0.5f
                                    val controlX3 = startX3 -20f
                                    val controlY3 = (startY3 + endY3) / 2
                                    val path3 = Path().apply {
                                        moveTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)
                                    }
                                    drawPath(
                                        path = path3,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth3)
                                    )
                                    //line right
                                    val startX4 = size.width
                                    val startY4 = 2f
                                    val endX4 = size.width
                                    val endY4 = size.height-2f
                                    val strokeWidth4 = 0.5f
                                    val controlX4 = startX4 +20f
                                    val controlY4 = (startY4 + endY4) / 2
                                    val path4 = Path().apply {
                                        moveTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                    }
                                    drawPath(
                                        path = path4,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth4)
                                    )
                                    //down
                                    val startX2 = 0f
                                    val startY2 = size.height -2f
                                    val endX2 = size.width
                                    val endY2 = size.height-2f
                                    val strokeWidth2 = 0.5f

                                    val controlX2 = (startX2 + endX2) / 2
                                    val controlY2 = startY2 + 20f

                                    val path2 = Path().apply {
                                        moveTo(startX2, startY2)
                                        quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                                    }

                                    drawPath(
                                        path = path2,
                                        color = Color.Black,
                                        style = Stroke(width = strokeWidth2)
                                    )


                                    val enclosingPath = Path().apply {
                                        moveTo(startX, startY)
                                        quadraticBezierTo(controlX, controlY, endX, endY)
                                        lineTo(startX4, startY4)
                                        quadraticBezierTo(controlX4, controlY4, endX4, endY4)
                                        lineTo(endX2, endY2)
                                        quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                                        lineTo(startX3, startY3)
                                        quadraticBezierTo(controlX3, controlY3, endX3, endY3)

                                        close()
                                    }

                                    drawPath(
                                        path = enclosingPath,
                                        color =if(selectedItem==4)Color(0xFFA21C4E) else Color(0xFF4B5166),
                                        style = Fill
                                    )
                                }
                                Icon(

                                    painter = painterResource(id = R.drawable.bb4),
                                    modifier =
                                    Modifier
                                        .size(25.dp),
                                    contentDescription = null,
                                    tint = Color.White
                                )

                            }

                        }
                        Spacer(modifier = Modifier.height(15.dp))
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {

                            Row(
                                verticalAlignment = Alignment.Bottom,

                                ) {
                                Text(
                                    text = "Last Price:",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Normal
                                )
                                Text(
                                    text = " 11110",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Medium
                                )
                                Text(
                                    text = ".23 USD",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 10.sp,
                                    fontWeight = FontWeight.W400
                                )


                            }
                            //s r2
                            Row(
                                verticalAlignment = Alignment.Bottom,

                                ) {
                                Text(
                                    text = "Low:",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Normal
                                )
                                Text(
                                    text = " 853",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Medium
                                )
                                Text(
                                    text = ".11 USD",
                                    color = MaterialTheme.colorScheme.onPrimary,
                                    fontFamily = Sora,
                                    fontSize = 10.sp,
                                    fontWeight = FontWeight.W400
                                )


                            }

                        }
                        //s
                        Spacer(modifier = Modifier.height(5.dp))

                        Row(
                            modifier = Modifier
                                .fillMaxWidth(),
                        ) {
                            Row(
                                verticalAlignment = Alignment.Bottom,

                                ) {
                                Text(
                                    text = "High:",
                                    color = Color(0xFF2CFF8C),
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Normal
                                )
                                Text(
                                    text = " 1495",
                                    color = Color(0xFF2CFF8C),
                                    fontFamily = Sora,
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Medium
                                )
                                Text(
                                    text = ".86 USD",
                                    color = Color(0xFF2CFF8C),
                                    fontFamily = Sora,
                                    fontSize = 10.sp,
                                    fontWeight = FontWeight.W400
                                )

                            }
                        }



                    }
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .padding(start = 5.dp),) {
                        Column(
                            Modifier
                                .fillMaxWidth()
                                .padding(start = 5.dp),
                        ) {
                            Row(Modifier.fillMaxWidth()) {
                                Text(
                                    text = "",
                                    color = Color(0xFFA3A5AC),
                                    fontFamily = Sora,
                                    fontSize = 6.sp,
                                    fontWeight = FontWeight.Normal,

                                    )
                            }



                        }
                        Box(Modifier.fillMaxSize()) {
                            Column(Modifier.padding(start = 3.dp)) {
                                Text(
                                    text = "2000",
                                    color = Color(0xFFA3A5AC),
                                    fontFamily = Sora,
                                    fontSize = 6.sp,
                                    fontWeight = FontWeight.Normal,
                                    modifier = Modifier.offset(y = -3.dp)
                                )
                                Spacer(modifier = Modifier.height(22.dp))
                                Text(
                                    text = "1500",
                                    color = Color(0xFFA3A5AC),
                                    fontFamily = Sora,
                                    fontSize = 6.sp,
                                    fontWeight = FontWeight.Normal,
                                )
                                Spacer(modifier = Modifier.height(26.dp))
                                Text(
                                    text = "1000",
                                    color = Color(0xFFA3A5AC),
                                    fontFamily = Sora,
                                    fontSize = 6.sp,
                                    fontWeight = FontWeight.Normal,
                                )
                                Spacer(modifier = Modifier.height(17.dp))
                                Text(
                                    text = "400",
                                    color = Color(0xFFA3A5AC),
                                    fontFamily = Sora,
                                    fontSize = 6.sp,
                                    fontWeight = FontWeight.Normal,
                                    modifier = Modifier.offset(x = 3.dp)
                                )

                                Spacer(modifier = Modifier.height(8.dp))
                                Row(Modifier.fillMaxWidth()){
                                    Spacer(modifier = Modifier.width(10.dp))
                                    Text(
                                        text = "Jun 20",
                                        color = Color(0xFFA3A5AC),
                                        fontFamily = Sora,
                                        fontSize = 10.sp,
                                        fontWeight = FontWeight.Normal,
                                    )
                                    Spacer(modifier = Modifier.width(25.dp))
                                    Text(
                                        text = "Jun 21",
                                        color = Color(0xFFA3A5AC),
                                        fontFamily = Sora,
                                        fontSize = 10.sp,
                                        fontWeight = FontWeight.Normal,
                                    )
                                    Spacer(modifier = Modifier.width(30.dp))
                                    Text(
                                        text = "Jun 22",
                                        color = Color(0xFFA3A5AC),
                                        fontFamily = Sora,
                                        fontSize = 10.sp,
                                        fontWeight = FontWeight.Normal,
                                    )
                                    Spacer(modifier = Modifier.width(26.dp))
                                    Text(
                                        text = "Jun 23",
                                        color = Color(0xFFA3A5AC),
                                        fontFamily = Sora,
                                        fontSize = 10.sp,
                                        fontWeight = FontWeight.Normal,
                                    )
                                    Spacer(modifier = Modifier.width(27.dp))
                                    Text(
                                        text = "Jun 24",
                                        color = Color(0xFFA3A5AC),
                                        fontFamily = Sora,
                                        fontSize = 10.sp,
                                        fontWeight = FontWeight.Normal,
                                    )

                                }


                            }

                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(start = 30.dp, bottom = 30.dp)
                            ) {

                                Spacer(modifier = Modifier.height(30.dp))


                                CustomRectangle()
                                /*
                                                            val path = Path()
                                                            path.moveTo(0f, 0f)
                                                            path.quadraticBezierTo(0f,0f,3f,3f)

                                 */


                            }

                        }



                    }




                    //s
                }
                //s
            }
        }


        

        Spacer(Modifier.height(30.dp))
        Box(
            Modifier
                .systemBarsPadding()
                .fillMaxWidth()
                .height(200.dp)
                .padding(start = 25.dp, end = 25.dp)
        ) {
            val cardcolor = MaterialTheme.colorScheme.primary

            var x by remember {
                mutableStateOf(false)
            }
            LaunchedEffect(Unit) {
                while (true) {
                    delay(3000L)
                    x = !x
                }
            }

            var y by remember {
                mutableStateOf(false)
            }
            LaunchedEffect(Unit) {
                while (true) {
                    delay(2000L)
                    y = !y
                }
            }
            var z by remember {
                mutableStateOf(false)
            }
            LaunchedEffect(Unit) {
                while (true) {
                    delay(1000L)
                    z = !z
                }
            }

            Canvas(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                val startX = 0f
                val startY = 30f
                val endX = size.width
                val endY = 30f
                val strokeWidth = 4f

                val controlX = (startX + endX) / 2
                val controlY = startY - 50f

                val path = Path().apply {
                    moveTo(startX, startY)
                    quadraticBezierTo(controlX, controlY, endX, endY)
                }

                drawPath(
                    path = path,
                    color = Color.Black,
                    style = Stroke(width = strokeWidth)
                )

                drawLine(
                    color = Color.Black,
                    start = Offset(0f, 30f),
                    end = Offset(0f, size.height-30),
                    strokeWidth = 4f
                )
                drawLine(
                    color = Color.Black,
                    start = Offset(size.width, 30f),
                    end = Offset(size.width, size.height-30),
                    strokeWidth = 4f
                )
                //s
                val startX2 = 0f
                val startY2 = size.height -30
                val endX2 = size.width
                val endY2 = size.height-30
                val strokeWidth2 = 4f

                val controlX2 = (startX2 + endX2) / 2
                val controlY2 = startY2 + 50f

                val path2 = Path().apply {
                    moveTo(startX2, startY2)
                    quadraticBezierTo(controlX2, controlY2, endX2, endY2)
                }

                drawPath(
                    path = path2,
                    color = Color.Black,
                    style = Stroke(width = strokeWidth2)
                )

                // Fill the enclosed area with a desired color
                val enclosingPath = Path().apply {
                    moveTo(startX, startY)
                    quadraticBezierTo(controlX, controlY, endX, endY)
                    lineTo(endX2, endY2)
                    quadraticBezierTo(controlX2, controlY2, startX2, startY2)
                    close()
                }

                drawPath(
                    path = enclosingPath,
                    color = cardcolor,
                    style = Fill
                )

            }
            Column(
                Modifier
                    .fillMaxSize()
                    .padding(top = 30.dp)
            ) {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(start = 30.dp)
                )
                {
                    Text(
                        text = "Portfolio",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontFamily = Sora,
                        fontSize = 11.sp,
                        fontWeight = FontWeight.Medium
                    )

                }
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(start = 20.dp, end = 30.dp,),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                )
                {
                    CircularSlider(modifier = Modifier.size(150.dp).padding(bottom = 17.dp))

                    Column(
                        Modifier
                            .fillMaxSize()
                            .padding(top = 20.dp)
                    ) {
                        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically)
                        {
                            Text(
                                text = "BTC = ",
                                color = Color(0xFFEA973D),
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )
                            Text(
                                text = "0.0087654",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )
                            Icon(

                                painter = painterResource(id =if(y) R.drawable.upc else R.drawable.downc ),
                                modifier =
                                Modifier
                                    .height(10.dp)
                                    .width(14.dp),
                                contentDescription = null,
                                tint =if(y) Color(0xFF21A64D) else Color(0xFFFC3257)


                            )

                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        //2
                        Row(
                            Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        )
                        {
                            Text(
                                text = "ETH = ",
                                color = Color(0xFF5E76FB),
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )
                            Text(
                                text = "1.456543",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )
                            Icon(

                                painter = painterResource(id =if(x) R.drawable.downc else R.drawable.upc ),
                                modifier =
                                Modifier
                                    .height(10.dp)
                                    .width(14.dp),
                                contentDescription = null,
                                tint =if(x) Color(0xFFFC3257) else Color(0xFF21A64D)

                            )

                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        //3
                        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically)
                        {
                            Text(
                                text = "XMR = ",
                                color = Color(0xFFEA6404),
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )
                            Text(
                                text = "0.123343",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )

                            Icon(

                                painter = painterResource(id =if(z) R.drawable.upc else R.drawable.downc ),
                                modifier =
                                Modifier
                                    .height(10.dp)
                                    .width(14.dp),
                                contentDescription = null,
                                tint =if(z) Color(0xFF21A64D) else Color(0xFFFC3257)


                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        //4
                        Row(
                            Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        )

                        {
                            Text(
                                text = "Others = ",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )
                            Text(
                                text = "0.00367",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )
                            Icon(

                                painter = painterResource(id =if(x) R.drawable.upc else R.drawable.downc ),
                                modifier =
                                Modifier
                                    .height(10.dp)
                                    .width(14.dp),
                                contentDescription = null,
                                tint =if(x) Color(0xFF21A64D) else Color(0xFFFC3257)


                            )
                        }

                    }
                }
            }
        }
    }
}
