package com.example.myapplication

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.tradeapp.R

@Composable
fun settingPage(navController: NavController) {
    Box(
        modifier = Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(MaterialTheme.colorScheme.surface)

    ) {
        Column(
            Modifier
                .background(MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .padding(start = 23.dp, top = 80.dp)
        )
        {

            //ex switch
            /*
            Box(
                modifier = Modifier
                    .width(339.dp)
                    .height(240.dp)
                    .background(MaterialTheme.colorScheme.primary),
                contentAlignment = Alignment.Center
                // .padding(20.dp)
            )
            {
                switch1()

            }
            */

            Box(
                modifier = Modifier
                    .width(339.dp)
                    .height(240.dp)
                    .clip(RoundedCornerShape(20.dp))
                    .background(MaterialTheme.colorScheme.primary)
                    .border(
                        1.dp, Color.Black, RoundedCornerShape(20.dp)
                    )

                // .padding(20.dp)
            )
            {
                Column(Modifier.fillMaxSize()) {

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = 20.dp, end = 40.dp, top = 20.dp),
                        horizontalArrangement = Arrangement.End
                    ) {
                        Text(
                            text = "Recommended",
                            color = Color(0xFF16E273),
                            fontFamily = Sora,
                            fontSize = 10.sp,
                            fontWeight = FontWeight.Normal
                        )

                    }
                    Spacer(modifier = Modifier.height(5.dp))
                    //s

                    Row(
                        modifier = Modifier
                            .padding(start = 20.dp, end = 20.dp,),
                        verticalAlignment = Alignment.CenterVertically,

                        ) {
                        Icon(

                            painter = painterResource(id = R.drawable.icons8_google_logo),
                            modifier =
                            Modifier
                                .size(30.dp),

                            contentDescription = null,
                            tint = Color(0xFFA3A5AC)

                        )
                        Spacer(modifier = Modifier.width(15.dp))
                        Text(
                            text = "Authenticator App",
                            color = MaterialTheme.colorScheme.onPrimary,
                            fontFamily = Sora,
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Medium
                        )
                        Spacer(modifier = Modifier.width(60.dp))
                        var Selector by remember { mutableStateOf(true) }

                        Box(
                            modifier = Modifier
                                .width(39.dp)
                                .height(23.dp)
                                .clip(RoundedCornerShape(16.dp))
                                .background(
                                    if (!Selector) Color(0xFF2CFF8C) else
                                        MaterialTheme.colorScheme.onSecondary
                                )
                                .border(
                                    1.dp,
                                    Color(0xFFD6D9E2),
                                    RoundedCornerShape(12.dp)
                                )
                        ) {
                            Row(Modifier.fillMaxSize(), horizontalArrangement = Arrangement.Start) {
                                if (Selector) {

                                    Box(
                                        modifier = Modifier
                                            .size(23.dp)
                                            .clip(RoundedCornerShape(16.dp))
                                            .background(MaterialTheme.colorScheme.onSecondary)
                                            .border(
                                                1.dp,
                                                Color(0xFFD6D9E2),
                                                RoundedCornerShape(12.dp)
                                            )
                                            .clickable { Selector = false }
                                    )
                                } else {
                                    Spacer(modifier = Modifier.width(16.dp))
                                    Box(
                                        modifier = Modifier
                                            .size(23.dp)
                                            .clip(RoundedCornerShape(16.dp))
                                            .background(Color.White)
                                            .border(
                                                1.dp,
                                                MaterialTheme.colorScheme.onSecondary,
                                                RoundedCornerShape(12.dp)
                                            )
                                            .clickable { Selector = true }
                                    )
                                }
                            }
                        }
                    }
                    //ss
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = 60.dp, top = 20.dp)

                    ) {
                        Row(
                            Modifier.fillMaxWidth(),
                        ) {
                            Text(
                                text = "prevent unauthorized acces and use for",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 10.sp,
                                fontWeight = FontWeight.Normal
                            )

                        }
                        //ss
                        Row(
                            Modifier.fillMaxWidth()
                        ) {
                            Text(
                                text = "withdrawals, and other security purposes",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 10.sp,
                                fontWeight = FontWeight.Normal
                            )

                        }

                    }

                    //ss
                    Spacer(modifier = Modifier.height(10.dp))

                    Divider(
                        color = Color(0xCC818181),
                        thickness = 1.dp,
                        modifier = Modifier
                            .fillMaxWidth()
                    )

                    Spacer(modifier = Modifier.height(10.dp))

                    //sss
                    Row(
                        modifier = Modifier
                            .padding(start = 20.dp, end = 20.dp,),
                        verticalAlignment = Alignment.CenterVertically,

                        ) {
                        Icon(

                            painter = painterResource(id = R.drawable.icons8_sms),
                            modifier =
                            Modifier
                                .size(27.dp),

                            contentDescription = null,
                            tint = Color(0xFFA3A5AC)

                        )
                        Spacer(modifier = Modifier.width(15.dp))
                        Text(
                            text = "Text Message",
                            color = MaterialTheme.colorScheme.onPrimary,
                            fontFamily = Sora,
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Medium
                        )
                        Spacer(modifier = Modifier.width(105.dp))
                        var Selector by remember { mutableStateOf(true) }

                        Box(
                            modifier = Modifier
                                .width(39.dp)
                                .height(23.dp)
                                .clip(RoundedCornerShape(16.dp))
                                .background(
                                    if (!Selector) Color(0xFF2CFF8C) else
                                        MaterialTheme.colorScheme.onSecondary
                                )
                                .border(
                                    1.dp,
                                    Color(0xFFD6D9E2),
                                    RoundedCornerShape(12.dp)
                                )
                        ) {
                            Row(Modifier.fillMaxSize(), horizontalArrangement = Arrangement.Start) {
                                if (Selector) {

                                    Box(
                                        modifier = Modifier
                                            .size(23.dp)
                                            .clip(RoundedCornerShape(16.dp))
                                            .background(MaterialTheme.colorScheme.onSecondary)
                                            .border(
                                                1.dp,
                                                Color(0xFFD6D9E2),
                                                RoundedCornerShape(12.dp)
                                            )
                                            .clickable { Selector = false }
                                    )
                                } else {
                                    Spacer(modifier = Modifier.width(16.dp))
                                    Box(
                                        modifier = Modifier
                                            .size(23.dp)
                                            .clip(RoundedCornerShape(16.dp))
                                            .background(Color.White)
                                            .border(
                                                1.dp,
                                                MaterialTheme.colorScheme.onSecondary,
                                                RoundedCornerShape(12.dp)
                                            )
                                            .clickable { Selector = true }
                                    )
                                }
                            }
                        }
                    }
                    //ss
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = 60.dp, top = 20.dp)

                    ) {
                        Row(
                            Modifier.fillMaxWidth(),
                        ) {
                            Text(
                                text = "prevent unauthorized acces and use for",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 10.sp,
                                fontWeight = FontWeight.Normal
                            )

                        }
                        //ss
                        Row(
                            Modifier.fillMaxWidth()
                        ) {
                            Text(
                                text = "withdrawals, and other security purposes",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 10.sp,
                                fontWeight = FontWeight.Normal
                            )

                        }

                    }

                }
            }
            // second box
            Spacer(modifier = Modifier.height(10.dp))

            Box(
                modifier = Modifier
                    .width(339.dp)
                    .height(110.dp)
                    .clip(RoundedCornerShape(20.dp))
                    .background(MaterialTheme.colorScheme.primary)
                    .border(
                        1.dp, Color.Black, RoundedCornerShape(20.dp)
                    )
                    .clickable { }
                // .padding(20.dp)
            )
            {
                Column(
                    Modifier
                        .fillMaxSize()
                        .padding(start = 20.dp, end = 20.dp, top = 10.dp),
                ) {
                    Row(
                        //   horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = "Password",
                            color = MaterialTheme.colorScheme.onPrimary,
                            fontFamily = Sora,
                            fontSize = 12.sp,
                            fontWeight = FontWeight.Medium,

                            )
                        Spacer(modifier = Modifier.width(10.dp))
                        Text(
                            text = "(Last Updated: 2019/12/01 09:20 PM)",
                            color = Color(0xFFA3A5AC),
                            fontFamily = Sora,
                            fontSize = 10.sp,
                            fontWeight = FontWeight.Normal
                        )

                    }
                    Spacer(modifier = Modifier.height(20.dp))

                    Row {
                        Text(
                            text = "Enhance your Security with a strong and unique",
                            color = Color(0xFFA3A5AC),
                            fontFamily = Sora,
                            fontSize = 10.sp,
                            fontWeight = FontWeight.Normal

                        )
                    }
                    Row {
                        Text(
                            text = "password for your Bitex account.",
                            color = Color(0xFFA3A5AC),
                            fontFamily = Sora,
                            fontSize = 10.sp,
                            fontWeight = FontWeight.Normal
                        )

                    }

                }

            }
            //s
            Spacer(modifier = Modifier.height(10.dp))
            Box(
                modifier = Modifier
                    .width(339.dp)
                    .height(130.dp)
                    .clip(RoundedCornerShape(20.dp))
                    .background(MaterialTheme.colorScheme.primary)
                    .border(
                        1.dp, Color.Black, RoundedCornerShape(20.dp)
                    )
                    .clickable { }
                // .padding(20.dp)
            )
            {

                Column (
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 20.dp, end = 20.dp, top = 10.dp),
                ){
                    Row(
                        modifier = Modifier
                            .padding(start = 78.dp)
                    )

                    {
                        Text(
                            text = "Preferred",
                            color = Color(0xFF2CFF8C),
                            fontFamily = Sora,
                            fontSize = 10.sp,
                            fontWeight = FontWeight.Medium
                        )

                    }
                    Text(
                        text = "Backup Codes",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Text(
                        text = "This feature lets you use 10 one-time code to log ",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(
                        text = "in or disable two-factor authentication when away",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(
                        text = "your phone.",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )



                }


            }
            //box 3
            Spacer(modifier = Modifier.height(10.dp))
            Box(
                modifier = Modifier
                    .width(339.dp)
                    .height(110.dp)
                    .clip(RoundedCornerShape(20.dp))
                    .background(MaterialTheme.colorScheme.primary)
                    .border(
                        1.dp, Color.Black, RoundedCornerShape(20.dp)
                    )
                    .clickable { }

                // .padding(20.dp)
            )
            {

                Column (
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 20.dp, end = 20.dp, top = 10.dp),
                ){

                    Text(
                        text = "Trusted IP Address",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Text(                                                       //
                        text = "This security feature lets you to control who can ",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(
                        text = "access your Bitex account based on their IP add",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(
                        text = "resses.",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )

                }
            }
            //ss

            Spacer(modifier = Modifier.height(10.dp))
            Box(
                modifier = Modifier
                    .width(339.dp)
                    .height(110.dp)
                    .clip(RoundedCornerShape(20.dp))
                    .background(MaterialTheme.colorScheme.primary)
                    .border(
                        1.dp, Color.Black, RoundedCornerShape(20.dp)
                    )
                    .clickable { }
                // .padding(20.dp)
            )
            {

                Column (
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 20.dp, end = 20.dp, top = 10.dp),
                ){

                    Text(
                        text = "Security Phrase",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Text(                                                       //
                        text = "you can review your account's security logs inclu ",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(                                                     //
                        text = "ding the information below to better undrestand",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(
                        text = "the actions you've performed in the last 90 days.",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )

                }
            }
            Spacer(modifier = Modifier.height(10.dp))
            Box(
                modifier = Modifier
                    .width(339.dp)
                    .height(110.dp)
                    .clip(RoundedCornerShape(20.dp))
                    .background(MaterialTheme.colorScheme.primary)
                    .border(
                        1.dp, Color.Black, RoundedCornerShape(20.dp)
                    )
                    .clickable { }
                // .padding(20.dp)
            )
            {

                Column (
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 20.dp, end = 20.dp, top = 10.dp),
                ){

                    Text(
                        text = "Security Events",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Text(                                                       //
                        text = "you can review your account's security logs inclu ",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(                                                     //
                        text = "ding the information below to better undrestand",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )
                    Text(
                        text = "the actions you've performed in the last 90 days.",
                        color = Color(0xFFA3A5AC),
                        fontFamily = Sora,
                        fontSize = 10.sp,
                        fontWeight = FontWeight.Normal
                    )

                }
            }
            Spacer(modifier = Modifier.height(20.dp))


        }

    }

}

@Composable
fun switch1(modifier: Modifier = Modifier) {
    var checked1 by remember { mutableStateOf(false) }
    Switch(checked = checked1 , onCheckedChange = {checked1 = it} ,
        colors = SwitchDefaults.colors(
            checkedIconColor = Color.White,
            checkedBorderColor = Color.Yellow,
            checkedThumbColor = Color.Red,
            uncheckedThumbColor = Color.Blue,
            checkedTrackColor = Color.Green,

            ),


        )

}

