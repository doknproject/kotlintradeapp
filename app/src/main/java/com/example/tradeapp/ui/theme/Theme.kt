package com.example.myapplication.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext

private val DarkColorScheme = darkColorScheme(
    primary = ThemeColors.Night.background,
    onPrimary = ThemeColors.Night.text,
    secondary = ThemeColors.Night.surface,
    tertiary = ThemeColors.Night.background,
    surface = ThemeColors.Night.surface,
    error = ThemeColors.Night.bottomController,
    onError = ThemeColors.Night.bottomColor,
    secondaryContainer = ThemeColors.Night.Secondary,
    onSecondary = ThemeColors.Night.OnSecondary
)

private val LightColorScheme = lightColorScheme(
    primary = ThemeColors.Day.background,
    onPrimary = ThemeColors.Day.text,
    secondary = ThemeColors.Day.surface,
    tertiary = ThemeColors.Day.background,
    surface = ThemeColors.Day.surface,
    error = ThemeColors.Day.bottomController,
    onError = ThemeColors.Day.bottomColor,
    secondaryContainer = ThemeColors.Day.Secondary,
    onSecondary = ThemeColors.Day.OnSecondary

)
@Composable
fun JukeBoxComposeTheme(
    isDarkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    MaterialTheme(
        typography = Typography,
        content = content,
        colorScheme = if (isDarkTheme) DarkColorScheme else lightColorScheme()
    )
}

/* Other default colors to override
background = Color(0xFFFFFBFE),
surface = Color(0xFFFFFBFE),
onPrimary = Color.White,
onSecondary = Color.White,
onTertiary = Color.White,
onBackground = Color(0xFF1C1B1F),
onSurface = Color(0xFF1C1B1F),
*/


@Composable
fun MyApplicationTheme(
    isDarkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    MaterialTheme(
        typography = Typography,
        content = content,
        colorScheme = if (isDarkTheme) DarkColorScheme else LightColorScheme
    )
}