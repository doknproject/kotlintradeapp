package com.example.myapplication

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.tradeapp.R

@Composable
fun BottomNavigation1(navController: NavController, showTopAppBar: Boolean = true, aa : MutableState<Int> ) {
    if (showTopAppBar) {

        BottomAppBar(
            contentPadding = PaddingValues(0.dp),
            modifier = Modifier
                .systemBarsPadding()
                .fillMaxWidth()
                .height(51.dp)
                .clip(RoundedCornerShape(topStart = 15.dp, topEnd = 15.dp)),


            containerColor = MaterialTheme.colorScheme.onError,

            ) {

            Row(
                modifier = Modifier
                    .fillMaxSize(),
                horizontalArrangement = Arrangement.SpaceEvenly,
                verticalAlignment = Alignment.CenterVertically
            )
            {

                // val currentRoute = navController.currentDestination?.route// this didnt help
                IconButton(
                    modifier = Modifier
                        .padding(0.dp), onClick = {
                        aa.value = 1
                        navController.navigate("HomePage"){
                            // popUpTo(currentRoute!!) { inclusive = true }
                        }
                    }) {
                    Icon(

                        painter = painterResource(id = R.drawable.b11),
                        modifier =
                        Modifier
                            .height(24.dp)
                            .width(20.dp),
                        contentDescription = null,
                        tint = if (aa.value == 1) MaterialTheme
                            .colorScheme.error
                        else Color(0xFFA3A5AC)

                    )
                }
                //2
                IconButton(
                    modifier = Modifier
                        .padding(0.dp), onClick = {
                        aa.value = 2
                        navController.navigate("onTradePage")
                        // navController.navigate("")
                    }) {
                    Icon(

                        painter = painterResource(id = R.drawable.b2),
                        modifier =
                        Modifier
                            .height(19.dp)
                            .width(28.dp),
                        contentDescription = null,
                        tint = if (aa.value == 2) MaterialTheme
                            .colorScheme.error
                        else Color(0xFFA3A5AC)


                    )
                }
                //3
                IconButton(
                    modifier = Modifier
                        .padding(0.dp), onClick = {
                        aa.value
                        navController.navigate("thirdPage")
                    }) {
                    Icon(

                        painter = painterResource(id = R.drawable.b3),
                        modifier =
                        Modifier
                            .size(23.dp),
                        contentDescription = null,
                        tint = if (aa.value==3) MaterialTheme
                            .colorScheme.error
                        else Color(0xFFA3A5AC)

                    )
                }
                //4
                IconButton(
                    modifier = Modifier
                        .padding(0.dp), onClick = {
                        aa.value = 4
                        navController.navigate("forthPage")
                    }) {
                    Icon(

                        painter = painterResource(id = R.drawable.b4),
                        modifier =
                        Modifier
                            .size(22.dp),
                        contentDescription = null,
                        tint = if (aa.value == 4) MaterialTheme
                            .colorScheme.error
                        else Color(0xFFA3A5AC)

                    )
                }
            }
        }
    }
}
//topAppBar
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun topAppBar(navController: NavController, showTopAppBar: Boolean = true,) {
    TopAppBar(modifier = Modifier
        .background(MaterialTheme.colorScheme.surface), title = {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(start= 10.dp ,end = 20.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically

        ) {
            if(!showTopAppBar){

                Icon(
                    painter = painterResource(id = R.drawable.left),
                    modifier =
                    Modifier
                        .width(12.dp)
                        .height(21.dp)
                        .clickable {
                            navController.popBackStack()
                        }
                    ,
                    contentDescription = null,
                    tint = Color(0xFFA3A5AC)
                )
            }
            //
            else{
                Spacer(modifier = Modifier.width(95.dp))
            }




            Row(
                Modifier.padding(
                    start = if (!showTopAppBar) 35.dp else 0.dp
                )
                ,
                horizontalArrangement = if (!showTopAppBar) Arrangement.SpaceEvenly
                else Arrangement.SpaceBetween
            )
            {
                Text(
                    text = "m",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontFamily = Sora,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = "u",
                    color = Color(0xFFFFC90E),
                    fontFamily = Sora,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = "tant",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontFamily = Sora,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
            }
            //s
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                if(showTopAppBar){
                    IconButton(
                        modifier = Modifier
                            .padding(0.dp), onClick = {
                            // controller = 1
                            navController.navigate("settingPage")
                        }) {
                        Icon(

                            painter = painterResource(id = R.drawable.shape__1_),
                            modifier =
                            Modifier
                                .size(18.dp),
                            contentDescription = null,
                            tint = Color(0xFFA3A5AC)
                        )
                    }
                }

                // Spacer(modifier = Modifier.width(5.dp))
                Box(
                    Modifier
                        .size(42.dp)
                        .clip(shape = RoundedCornerShape(20.dp))
                        // .border(1.dp, Color.Gray, RoundedCornerShape(12.dp))
                        .background(Color.Gray)
                ) {
                    androidx.compose.foundation.Image(
                        painter = painterResource(id = R.drawable.prof1),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        alignment = Alignment.Center
                    )
                }
            }
        }
    }

    )

}




