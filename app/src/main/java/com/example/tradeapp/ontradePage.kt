package com.example.myapplication

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.tradeapp.R

@Composable
fun MyGrid(modifier: Modifier = Modifier) {
    val cardsData: List<Map<String, Any>> = listOf(
        mapOf(
            "Num1" to R.drawable.jet,
            "Num1.5" to R.drawable.b,
            "Num2" to "XML/BTC",
            "Num3" to "1090.90076 XML",
            "Num4" to "0.00112300 BTC",
            "Num5" to 1f,
            "r1" to Color(0xFF1E68E5),
            "r2" to Color(0xFF4BB4E3),
            "r3" to Color(0xFFE8953C),
            "regnum" to "100%"
        ),
        mapOf(
            "Num1" to R.drawable.flower,
            "Num1.5" to R.drawable.du,
            "Num2" to "PPC/ETH",
            "Num3" to "1090.90076 PPC",
            "Num4" to "0.0011,2300 ETH",
            "Num5" to 0.7f,
            "r1" to Color(0xFFFFC90E),
            "r2" to Color(0xFF00AF45),
            "r3" to Color(0xFF657CFD),
            "regnum" to "70%"
        ),
        mapOf(
            "Num1" to R.drawable.m,
            "Num1.5" to R.drawable.du,
            "Num2" to "XMR/ETH",
            "Num3" to "1090.90076 XMR",
            "Num4" to "0.00112300 ETH",
            "Num5" to 0.5f,
            "r1" to Color(0xFFFF2C7B),
            "r2" to Color(0xFFF86902),
            "r3" to Color(0xFF657CFD),
            "regnum" to "50%"
        ),

        mapOf(
            "Num1" to R.drawable.jet,
            "Num1.5" to R.drawable.b,
            "Num2" to "XML/BTC",
            "Num3" to "1090.90076 XML",
            "Num4" to "0.00112300 BTC",
            "Num5" to 1f,
            "r1" to Color(0xFF1E68E5),
            "r2" to Color(0xFF4BB4E3),
            "r3" to Color(0xFFE8953C),
            "regnum" to "100%"
        ),
        mapOf(
            "Num1" to R.drawable.flower,
            "Num1.5" to R.drawable.du,
            "Num2" to "PPC/ETH",
            "Num3" to "1090.90076 PPC",
            "Num4" to "0.00112300 ETH",
            "Num5" to 0.7f,
            "r1" to Color(0xFFFFC90E),
            "r2" to Color(0xFF00AF45),
            "r3" to Color(0xFF657CFD),
            "regnum" to "70%"

        ),
        mapOf(
            "Num1" to R.drawable.m,
            "Num1.5" to R.drawable.du,
            "Num2" to "XMR/ETH",
            "Num3" to "1090.90076 XMR",
            "Num4" to "0.00112300 ETH",
            "Num5" to 0.5f,
            "r1" to Color(0xFFFF2C7B),
            "r2" to Color(0xFFF86902),
            "r3" to Color(0xFF657CFD),
            "regnum" to "50%"
        ),
        mapOf(
            "Num1" to R.drawable.jet,
            "Num1.5" to R.drawable.b,
            "Num2" to "XMR/ETH",
            "Num3" to "1090.90076 XMR",
            "Num4" to "0.00112300 ETH",
            "Num5" to 1f,
            "r1" to Color(0xFF1E68E5),
            "r2" to Color(0xFF4BB4E3),
            "r3" to Color(0xFFE8953C),
            "regnum" to "100%"
        ),
        mapOf(
            "Num1" to R.drawable.flower,
            "Num1.5" to R.drawable.du,
            "Num2" to "XMR/ETH",
            "Num3" to "1090.90076 XMR",
            "Num4" to "0.00112300 ETH",
            "Num5" to 0.7f,
            "r1" to Color(0xFFFFC90E),
            "r2" to Color(0xFF00AF45),
            "r3" to Color(0xFF657CFD),
            "regnum" to "70%"
        ),


        )

    MyScreen(cardsData = cardsData)


}

/*
  androidx.compose.foundation.Image(
                painter = painterResource(id = R.drawable.c1),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                alignment = Alignment.Center
            )
 */
@Composable
fun MyScreen(cardsData: List<Map<String, Any>>) {
    LazyVerticalGrid(columns = GridCells.Fixed(1),
        //verticalArrangement = Arrangement.spacedBy(16.dp),
        // fasele do onsor be sorat ofoghi ziad mishe
        //  horizontalArrangement = Arrangement.spacedBy(0.dp),
        // contentPadding = PaddingValues(30.dp),

    ) {
        items(cardsData) { map ->
            myCard(map)
        }
    }//
}
@SuppressLint("SuspiciousIndentation")
@Composable
fun myCard(map: Map<String, Any>,modifier: Modifier = Modifier) {
    var visible by remember {
        mutableStateOf(true)
    }


   // AnimatedVisibility(
      //  visible = visible,
      //  enter = expandHorizontally() + fadeIn(),
      //  exit = shrinkHorizontally() + fadeOut()
    //){
    val animatedPadding by animateDpAsState(
        if (visible) {
            18.dp
        } else {
            0.dp
        },
        label = "padding"
    )
    /*
    Box (  modifier.
        aspectRatio(1f)
        .fillMaxWidth()
        .padding(animatedPadding)
        .clip(RoundedCornerShape(20.dp))
        .background(MaterialTheme.colorScheme.primary)
        .border(
            1.dp,
            Color.Black,
            RoundedCornerShape(20.dp)
        )){

    }

     */
        Box(
            modifier = Modifier
              .fillMaxWidth()
                // .width(339.dp)
                .padding(start = animatedPadding, end = animatedPadding, bottom = 10.dp)
                .clip(RoundedCornerShape(20.dp))
                .background(MaterialTheme.colorScheme.primary)
                .border(
                    1.dp,
                    Color.Black,
                    RoundedCornerShape(20.dp)
                )
                .clickable { visible = !visible }
        ) {
            Column(
                modifier = Modifier.padding(20.dp)

            ) {
                Row(
                    Modifier.fillMaxWidth()
                ) {
                    Row(
                        verticalAlignment = Alignment.Bottom
                    ) {
                        val boxcolor1 = map["r2"] as Color
                        Box(
                            modifier = Modifier
                                .size(30.dp)
                                .clip(RoundedCornerShape(20.dp))
                                .background(boxcolor1)
                                .border(
                                    1.dp, Color.White, RoundedCornerShape(30.dp)
                                ),
                            contentAlignment = Alignment.Center

                        ){
                            val icon1 = map["Num1"] as Int
                            Icon(

                                painter = painterResource(id = icon1),
                                modifier =
                                Modifier
                                    .size(18.dp),
                                contentDescription = null,
                                tint = Color.White
                            )

                        }
                        Row(modifier = Modifier.offset(x=-10.dp,y=5.dp)) {
                            var boxcolor2 = map["r3"] as Color
                            val icon2 = map["Num1.5"] as Int
                            Box(
                                modifier = Modifier
                                    .size(19.dp)
                                    .clip(RoundedCornerShape(19.dp))
                                    .background(boxcolor2)
                                    .border(
                                        1.dp, Color.White, RoundedCornerShape(19.dp)
                                    ),
                                contentAlignment = Alignment.Center
                            ){
                                //  val icon1 = map["num1"] as? Int
                                Icon(

                                    painter = painterResource(id = icon2),
                                    modifier =
                                    Modifier
                                        .size(14.dp),
                                    contentDescription = null,
                                    tint = Color.White
                                )

                            }

                        }


                    }
//bbin
                    Column {
                        //val regColor: Color = map["r1"] as? Color ?: Color(0xFFFFC90E)
                        val regColor = map["r1"] as Color
                        Row(
                            Modifier.fillMaxWidth()
                            ,
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            val pf = map["Num5"] as Float
                            Text(
                                text = map["Num2"] as String,
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            LinearProgressIndicator(
                                progress = pf ,
                                modifier = Modifier
                                    .size(90.dp, 5.dp)
                                    .clip(
                                        RoundedCornerShape(100)
                                    ),
                                color = regColor                        )
                            Text(
                                text = map["regnum"] as String,
                                color = regColor,
                                fontFamily = Sora,
                                fontSize = 11.sp,
                                fontWeight = FontWeight.Normal
                            )

                        }
                        //s
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,

                            ) {

                            Text(
                                text = map["Num3"] as String,
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 10.sp,
                                fontWeight = FontWeight.Thin
                            )

                            Text(
                                text = map["Num4"] as String,
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 10.sp,
                                fontWeight = FontWeight.Thin
                            )

                            Box(
                                modifier = Modifier
                                    .size(18.dp)
                                    .clip(RoundedCornerShape(18.dp))
                                    .background(Color(0xFF4B5166)),
                                contentAlignment = Alignment.Center
                            ) {
                                if(visible)   Icon(

                                    painter = painterResource(R.drawable.down),
                                    modifier =
                                    Modifier
                                        .width(15.dp)
                                        .height(25.dp),
                                    contentDescription = null,
                                    tint = Color.White

                                )
                                else
                                    Icon(

                                        painter = painterResource(R.drawable.top),
                                        modifier =
                                        Modifier
                                            .width(15.dp)
                                            .height(25.dp),

                                        contentDescription = null,
                                        tint = Color.White

                                    )

                            }
                        }
                    }
                }
                AnimatedVisibility(!visible) {
                    Column(Modifier.fillMaxWidth()) {
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Order ID :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Text(
                                text = "0001320#4e5",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                        // row 2
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Price :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Text(
                                text = "0.002300 BTC",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                        //3
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Price :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Text(
                                text = "0.002300 BTC",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                        //3
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Date :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Text(
                                text = "2018.01.24 at 08:04:05",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                        //4
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Requested :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Text(
                                text = "150",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                        //5
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Canceled :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Text(
                                text = "10 ( 10% )",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                        //6
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Filled :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Text(
                                text = "140 (90%)",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal
                            )
                        }
                        //7
                        Row(
                            Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween

                        ){
                            Text(
                                text = "Method :",
                                color = MaterialTheme.colorScheme.onPrimary,
                                fontFamily = Sora,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Medium
                            )
                            Row {
                                var selected by remember {
                                    mutableStateOf(2)
                                }
                                Box(
                                    modifier = Modifier
                                        //  .width(46.dp)
                                        .height(22.dp)
                                        .clip(RoundedCornerShape(5.dp))
                                        .clickable { selected = 1 }
                                        .background(if (selected == 1) Color(0xFF2CFF8C) else Color.Transparent),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Text(
                                        text = "Mobile",
                                        color =if(selected ==1 ) Color.Black else Color(0xFF4B5166),
                                        fontFamily = Sora,
                                        fontSize = 14.sp,
                                        fontWeight = FontWeight.Medium
                                    )
                                }
                                Spacer(modifier = Modifier.width(20.dp))
                                //2
                                Box(
                                    modifier = Modifier
                                        // .width(46.dp)
                                        .height(22.dp)
                                        .clip(RoundedCornerShape(5.dp))
                                        .clickable { selected = 2 }
                                        .background(if (selected == 2) Color(0xFF2CFF8C) else Color.Transparent),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Text(
                                        text = "Web",
                                        color =if(selected ==2 ) Color.Black else Color(0xFF4B5166),
                                        fontFamily = Sora,
                                        fontSize = 14.sp,
                                        fontWeight = FontWeight.Medium
                                    )
                                }
                                Spacer(modifier = Modifier.width(20.dp))
                                //3
                                Box(
                                    modifier = Modifier
                                        //     .width(46.dp)
                                        .height(22.dp)
                                        .clip(RoundedCornerShape(20.dp))
                                        .clickable { selected = 3 }
                                        .background(if (selected == 3) Color(0xFF2CFF8C) else Color.Transparent),
                                    contentAlignment = Alignment.Center

                                ) {
                                    Text(
                                        text = "API",
                                        color =if(selected ==3 ) Color.Black else Color(0xFF4B5166),
                                        fontFamily = Sora,
                                        fontSize = 14.sp,
                                        fontWeight = FontWeight.Medium
                                    )
                                }
                            }

                        }

                    }



                }


            }

        }

    }


//}
@Composable
fun onTradePage(navController: NavController) {
    Column(
        Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.surface)
            .padding( top = 80.dp, bottom = 60.dp)
    ) {
        MyGrid()

    }

}